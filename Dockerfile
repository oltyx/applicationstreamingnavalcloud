FROM oltyalex/ubuntu-janos-server:v0.2
#websocket port
EXPOSE 8188
#admin page ports http/https
EXPOSE 7088 7889
#REST API ports http/https
EXPOSE 8088 8089
#streaming video port
EXPOSE 8004
#streaming data port
EXPOSE 8012
