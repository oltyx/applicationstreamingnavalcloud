package render;

import com.aurellem.capture.Capture;
import com.aurellem.capture.IsoTimer;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jme3.app.BasicProfilerState;
import com.jme3.app.DebugKeysAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.app.StatsAppState;
import com.jme3.app.state.ScreenshotAppState;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.base.DefaultServer;
import com.jme3.network.serializing.Serializer;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.system.NanoTimer;
import com.jme3.util.TangentBinormalGenerator;
import com.simsilica.lemur.GuiGlobals;
import com.simsilica.lemur.OptionPanelState;
import com.simsilica.lemur.style.BaseStyles;
import demo.MainMenuState;
import interactivity.JsonMessage;
import latency.LatencyBinder;
import latency.LatencyTracker;
import org.apache.log4j.BasicConfigurator;
import org.json.JSONObject;
import org.lwjgl.opengl.Display;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * This is the Radar Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls
 * @author Alexandru Olteanu
 */
public class Radar extends SimpleApplication {
    static Injector injector;
    static LatencyTracker latencyTracker;
    
    public static void main(String[] args) throws IOException{
        injector = Guice.createInjector(new LatencyBinder());
        latencyTracker = injector.getInstance(LatencyTracker.class);
        SimpleApplication radarUI = new Radar();

        radarUI.setTimer(new IsoTimer(60)); //record at specfied FPS
        radarUI.setShowSettings(false);
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1920, 1080);
        radarUI.setSettings(settings);

        if(args.length != 3) {
            System.err.println("3 arguments have to be provided:");
            System.err.println("[IP_VIDEO] [PORT_VIDEO] [PORT_DATA]");
            System.exit(1);
        }
        //In case you want to debug to a .mp4 .avi or PNG screenshots
        //File video = File.createTempFile("JME-recording.", args[0]);
        Capture.captureVideo(radarUI, args[0], args[1], Integer.valueOf(args[2]));
        //System.out.println(video.getCanonicalPath());
        radarUI.start();
    }

    public Radar() {
        super(new StatsAppState(), new DebugKeysAppState(), new BasicProfilerState(false),
                new OptionPanelState(), // from Lemur
                new MainMenuState(),
                new ScreenshotAppState("", System.currentTimeMillis()));
    }

    private Geometry sphereOne;
    private Geometry sphereTwo;
    private Map<ScreenCorner, Vector3f> screenCorners;
    private Geometry mark;
    private Node shootables;

    @Override
    public void simpleInitApp() {
        //Initialize logger
        BasicConfigurator.configure();

        GuiGlobals.initialize(this);

        GuiGlobals globals = GuiGlobals.getInstance();
        BaseStyles.loadGlassStyle();
        globals.getStyles().setDefaultStyle("glass");

        DefaultServer server = null;
        try {
            server = (DefaultServer) Network.createServer(5300);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.addMessageListener(new MessageListener<HostedConnection>() {
            @Override
            public void messageReceived(HostedConnection hostedConnection, Message message) {
                System.out.println("Message received: " + message);
                JSONObject obj = new JSONObject(message.toString());
                int newX = obj.getInt("x");
                int newY = obj.getInt("y");
                shootActionBrowser(obj);
            }
        });
        Serializer.registerClass(JsonMessage.class);
        server.start();

        initKeys();       // load custom key mappings
        initMark();       // a red sphere to mark the hit

        //Create sphere 1
        Sphere sphereMeshOne = new Sphere(32, 32 , 0.5f);
        sphereOne = new Geometry("Sphere1", sphereMeshOne);
        Material sphereMatOne = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        sphereMatOne.setColor("Color", ColorRGBA.Green);
        sphereOne.setMaterial(sphereMatOne);
        sphereOne.move(-3f, 0f, 0f);
        
        //Create sphere 2
        Sphere sphereMeshTwo = new Sphere(32, 32, 0.5f);
        sphereTwo = new Geometry("Sphere2", sphereMeshTwo);
        Material sphereMatTwo = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        sphereMatTwo.setColor("Color", ColorRGBA.Green);
        sphereTwo.move(3f, 0f, 0f);
        sphereTwo.setMaterial(sphereMatTwo);

        //Get world coordinates from screen position
        screenCorners = new HashMap<>();
        float viewToProjectionZ = cam.getViewToProjectionZ(9.0f);
        Vector3f topLeft = cam.getWorldCoordinates(new Vector2f(0, Display.getHeight()), viewToProjectionZ);
        Vector3f topRight = cam.getWorldCoordinates(new Vector2f(Display.getWidth(), Display.getHeight()), viewToProjectionZ);
        Vector3f bottomRight = cam.getWorldCoordinates(new Vector2f(Display.getWidth(), 0), viewToProjectionZ);
        Vector3f bottomLeft = cam.getWorldCoordinates(new Vector2f(0, 0), viewToProjectionZ);
        screenCorners.put(ScreenCorner.TopLeft, topLeft);
        screenCorners.put(ScreenCorner.TopRight, topRight);
        screenCorners.put(ScreenCorner.BottomRight, bottomRight);
        screenCorners.put(ScreenCorner.BottomLeft, bottomLeft);
        
        shootables = new Node("Shootables");
        shootables.attachChild(sphereOne);
        shootables.attachChild(sphereTwo);
        
        rootNode.attachChild(shootables);
                
    }

    /**
     * Declaring the "Shoot" action and mapping to its triggers.
     */
    private void initKeys() {
        inputManager.addMapping("Shoot",
                new KeyTrigger(KeyInput.KEY_SPACE), // trigger 1: spacebar
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "Shoot");
    }
    
    /**
     * Defining the "Shoot" action: Determine what was hit and how to respond.
     */
    private ActionListener actionListener = new ActionListener() {

        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("Shoot") && !keyPressed) {
                shootActionApp();
            }
        }
    };

    private void shootActionBrowser(JSONObject jsonObject){
        int x = jsonObject.getInt("x");
        int wrongY = jsonObject.getInt("y");
        int y = Display.getHeight() - wrongY;
        Vector2f appVector = new Vector2f(x, y);
        shootAction(appVector);
        latencyTracker.setBrowserClickTime(jsonObject.getLong("click-timestamp"));
    }

    private void shootActionApp(){
        shootAction(inputManager.getCursorPosition().clone());
    }

    public void shootAction(Vector2f click2d){
        System.out.println("Click coordinates: " + click2d);
        NanoTimer collisionTimer = new NanoTimer();
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        Vector3f click3d = cam.getWorldCoordinates(
                click2d, 0f).clone();
        Vector3f dir = cam.getWorldCoordinates(
                click2d, 1f).subtractLocal(click3d).normalizeLocal();
        Ray ray = new Ray(click3d, dir);
        shootables.collideWith(ray, results);
        // 4. Print the results
        System.out.println("----- Collisions? " + results.size() + "-----");
        for (int i = 0; i < results.size(); i++) {
            // For each hit, we know distance, impact point, name of geometry.
            float dist = results.getCollision(i).getDistance();
            Vector3f pt = results.getCollision(i).getContactPoint();
            String hit = results.getCollision(i).getGeometry().getName();
            System.out.println("* Collision #" + i);
            System.out.println("  You shot " + hit + " at " + pt + ", " + dist + " wu away.");
        }
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
            // The closest collision point is what was truly hit:
            CollisionResult closest = results.getClosestCollision();
            // Let's interact - we mark the hit with a red dot.
            mark.setLocalTranslation(closest.getContactPoint());
            rootNode.attachChild(mark);
            System.out.println("Time from click to render: " + collisionTimer.getTimeInSeconds() + "\n");
        } else {
            // No hits? Then remove the red mark.
            rootNode.detachChild(mark);
        }
    }

    /**
     * A red ball that marks the last spot that was "hit" by the "shot".
     */
    protected void initMark() {
        
        Sphere sphereMesh = new Sphere(32, 32, 0.5f);
        mark = new Geometry("Shiny rock", sphereMesh);
        sphereMesh.setTextureMode(Sphere.TextureMode.Projected); // better quality on spheres
        TangentBinormalGenerator.generate(sphereMesh);           // for lighting effect
        Material sphereMat = new Material(assetManager,
                "Common/MatDefs/Light/Lighting.j3md");
        sphereMat.setTexture("DiffuseMap",
                assetManager.loadTexture("Textures/Terrain/Pond/Pond.jpg"));
        sphereMat.setTexture("NormalMap",
                assetManager.loadTexture("Textures/Terrain/Pond/Pond_normal.png"));
        sphereMat.setBoolean("UseMaterialColors", true);
        sphereMat.setColor("Diffuse", ColorRGBA.White);
        sphereMat.setColor("Specular", ColorRGBA.White);
        sphereMat.setFloat("Shininess", 64f);  // [0,128]
        mark.setMaterial(sphereMat);

        /**
         * Must add a light to make the lit object visible!
         */
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(1, 0, -2).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
    }

    float totalTime = 0f;
    Vector3f direction = Vector3f.UNIT_X;
    float magnitude = 1f;
    Random randomizer = new Random(System.currentTimeMillis());

    @Override
    public void simpleUpdate(float tpf) {
        latencyTracker.setRenderingTimeStart(System.currentTimeMillis());
        //Count total time
        totalTime += tpf;
        
        //Pulsate -> grow and shrink the spheres
        float timeInSec = timer.getTimeInSeconds();
        float initScale = 1;
        float amplitude = 0.3f;
        float angularFrequency = 3;
        float scale = initScale + amplitude * FastMath.sin(timeInSec * angularFrequency);
        sphereOne.setLocalScale(scale);
        sphereTwo.setLocalScale(scale);
        
        //Random move
        Vector3f forward = sphereOne.getLocalRotation().mult(direction);
        Vector3f currentLoc = sphereOne.getLocalTranslation();
        
        float bannedXright, bannedYright, bannedXleft, bannedYleft;
        bannedXright = screenCorners.get(ScreenCorner.TopRight).x;
        bannedYright = screenCorners.get(ScreenCorner.TopRight).y;
        bannedXleft = screenCorners.get(ScreenCorner.BottomLeft).x;
        bannedYleft = screenCorners.get(ScreenCorner.BottomLeft).y;
        float margin = 0.5f;
        
        //Keep the sphere within the camera view
        if(Math.abs(currentLoc.x - bannedXleft) < margin || Math.abs(currentLoc.x + bannedXleft) < margin){
            magnitude = -magnitude;
        } else if(Math.abs(currentLoc.x - bannedXright) < margin || Math.abs(currentLoc.x + bannedXright) < margin) {
            magnitude = -magnitude;
        } else if(Math.abs(currentLoc.y - bannedYleft) < margin || Math.abs(currentLoc.y + bannedYleft) < margin) {
            magnitude = -magnitude;
            
        } else if(Math.abs(currentLoc.y - bannedYright) < margin || Math.abs(currentLoc.y + bannedYright) < margin) {
            magnitude = -magnitude;
        }
           
        //change direction randomly
        sphereOne.move(forward.mult(tpf).mult(speed).mult(magnitude));
        if (totalTime > 1.5){ //every 1.5s change direction
            Integer randomNumber = randomizer.nextInt(4);
            switch(randomNumber){
                case 0:
                    direction = Vector3f.UNIT_X;
                    break;
                case 1:
                    direction = Vector3f.UNIT_Y;
                    break;
                case 2:
                    direction = new Vector3f(1, 1, 0); //diagonnaly 1st and 3rd quadrant
                    break;
                case 3:
                    direction = new Vector3f(-1, 1, 0); //diagonnaly 2nd and 4th quadrant
                    break;
                
                default: //Debugging purposes
                    System.out.println("Random number: " + randomNumber);
                    System.out.println("Direction: " + direction);
                    System.out.println("Magnitude: " + magnitude + "\n");
            }
            magnitude = randomNumber >= 2 ? 1 : -1; // positive and negative way to move
            totalTime = 0;
        }
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private enum ScreenCorner {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}
