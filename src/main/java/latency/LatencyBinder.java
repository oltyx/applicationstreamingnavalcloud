package latency;

import com.google.inject.AbstractModule;

public class LatencyBinder extends AbstractModule{

    @Override
    protected void configure() {
        bind(LatencyTracker.class).toInstance(LatencyTracker.getInstance());
    }
}
