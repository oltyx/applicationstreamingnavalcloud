package latency;

public class LatencyTracker {

    private long renderingStartTime;
    private long recordingStartTime;
    private long browserClickTime;

    private static LatencyTracker singleInstance = null;

    private LatencyTracker() {
    }

    public static LatencyTracker getInstance() {
        if(singleInstance == null){
            singleInstance = new LatencyTracker();
        }
        return singleInstance;
    }

    public long getRenderingStartTime(){
        return renderingStartTime;
    }

    public long getRecordingStartTime(){
        return recordingStartTime;
    }

    public long setRenderingTimeStart(long renderingTimeStart){
        this.renderingStartTime = renderingTimeStart;
        return renderingTimeStart;
    }

    public long setRecordingTimeStart(long recordingTimeStart){
        this.recordingStartTime = recordingTimeStart;
        return recordingTimeStart;
    }

    public long getBrowserClickTime() {
        return browserClickTime;
    }

    public void setBrowserClickTime(long browserClickTime) {
        this.browserClickTime = browserClickTime;
    }

    @Override
    public String toString() {
        return "LatencyTracker{" +
                "renderingStartTime=" + renderingStartTime +
                ", recordingStartTime=" + recordingStartTime +
                ", browserClickTime=" + browserClickTime +
                '}';
    }
}
