package interactivity;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class JsonMessage extends AbstractMessage{

    private String jsonString;

    public JsonMessage() {

    }

    public JsonMessage(String jsonString){
        this.jsonString = jsonString;
    }

    @Override
    public String toString() {
        return jsonString;
    }
}
