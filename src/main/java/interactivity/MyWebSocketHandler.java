package interactivity;

import com.jme3.network.Network;
import com.jme3.network.base.DefaultClient;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.IOException;

@WebSocket
public class MyWebSocketHandler {

    DefaultClient client;

    public MyWebSocketHandler() {
        try {
            client = (DefaultClient) Network.connectToServer("localhost", 5300, 5300);
        } catch (IOException e) {
            e.printStackTrace();
        }
        client.start();
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());

    }

    @OnWebSocketMessage
    public void onMessage(String message) {
        System.out.println("Message received in WebSocket: " + message);
        client.send(new JsonMessage(message));
    }
}