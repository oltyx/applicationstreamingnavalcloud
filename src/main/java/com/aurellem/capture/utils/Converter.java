package com.aurellem.capture.utils;

import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameConverter;

import java.nio.Buffer;
import java.nio.ByteBuffer;
public class Converter {

    private static class ByteBufferToFrame extends FrameConverter<ByteBuffer> {

        private int width;

        private int height;

        @Override
        public Frame convert(ByteBuffer byteBuffer) {
            Frame frame = new Frame();
            frame.imageWidth = width;
            frame.imageHeight = height;
            frame.imageChannels = 4;
            frame.imageDepth = Frame.DEPTH_BYTE;
            frame.imageStride = frame.imageChannels * width * Math.abs(frame.imageDepth) / 8;

            // Code taken from com.jme3.util.Screenshots
            byte[] cpuArray = new byte[width*height*frame.imageChannels];
            // copy native memory to java memory
            byteBuffer.clear();
            byteBuffer.get(cpuArray);
            byteBuffer.clear();
            int total = 0;
            // calcuate half of height such that all rows of the array are written to
            // e.g. for odd heights, write 1 more scanline
            int heightdiv2ceil = height % 2 == 1 ? (height / 2) + 1 : height / 2;
            for (int y = 0; y < heightdiv2ceil; y++){
                for (int x = 0; x < width; x++){
                    total++;
                    int inPtr  = (y * width + x) * 4;
                    int outPtr = ((height-y-1) * width + x) * 4;

                    byte b1 = cpuArray[inPtr+0];
                    byte g1 = cpuArray[inPtr+1];
                    byte r1 = cpuArray[inPtr+2];
                    byte a1 = cpuArray[inPtr+3];

                    byte b2 = cpuArray[outPtr+0];
                    byte g2 = cpuArray[outPtr+1];
                    byte r2 = cpuArray[outPtr+2];
                    byte a2 = cpuArray[outPtr+3];

                    cpuArray[outPtr+0] = b1;
                    cpuArray[outPtr+1] = g1;
                    cpuArray[outPtr+2] = r1;
                    cpuArray[outPtr+3] = a1;

                    cpuArray[inPtr+0] = b2;
                    cpuArray[inPtr+1] = g2;
                    cpuArray[inPtr+2] = r2;
                    cpuArray[inPtr+3] = a2;
                }
            }

            frame.image = new Buffer[] { ByteBuffer.wrap(cpuArray) };
            return frame;
        }

        @Override
        public ByteBuffer convert(Frame frame) {
            return null;
        }
    }

    public static Frame convert(ByteBuffer byteBuffer, int width, int height){
        ByteBufferToFrame byteBufferToFrame = new ByteBufferToFrame();
        byteBufferToFrame.height = height;
        byteBufferToFrame.width = width;
        return byteBufferToFrame.convert(byteBuffer);
    }

}
