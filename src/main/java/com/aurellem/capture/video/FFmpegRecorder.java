package com.aurellem.capture.video;

import com.aurellem.capture.utils.Converter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import latency.LatencyBinder;
import latency.LatencyTracker;
import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameRecorder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Objects;

public class FFmpegRecorder extends AbstractVideoRecorder{


    LatencyTracker latencyTracker;
    FFmpegFrameRecorder recorder;
    Frame frame;
    boolean videoReady = false;
    long currentTimeStamp = 0;

    int current;
    File outDir;
    String formatName = "png";


    public FFmpegRecorder(String ipAddress, String portVideo, int portData) {
        super(ipAddress, portVideo, portData);
        Injector injector = Guice.createInjector(new LatencyBinder());
        latencyTracker = injector.getInstance(LatencyTracker.class);
    }

    public void initVideo() {
        recorder = new FFmpegFrameRecorder("rtp://"+ IP_VIDEO +":"+ PORT_VIDEO, width, height);
        // decrease "startup" latency in FFMPEG (see:
        // https://trac.ffmpeg.org/wiki/StreamingGuide)
        recorder.setVideoOption("tune", "zerolatency");
        recorder.setOption("payload_type", "96");
        // tradeoff between quality and encode speed
        // possible values are ultrafast,superfast, veryfast, faster, fast,
        // medium, slow, slower, veryslow
        // ultrafast offers us the least amount of compression (lower encoder
        // CPU) at the cost of a larger stream size
        // at the other end, veryslow provides the best compression (high
        // encoder CPU) while lowering the stream size
        // (see: https://trac.ffmpeg.org/wiki/Encode/H.264)
        recorder.setVideoOption("preset", "ultrafast");
        // Constant Rate Factor (see: https://trac.ffmpeg.org/wiki/Encode/H.264)
        recorder.setVideoOption("crf", "28");
        // 2000 kb/s, reasonable "sane" area for 720
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        recorder.setFormat("rtp");
        // FPS (frames per second)
        this.setFps(60);
        recorder.setFrameRate(60);
        // Key frame interval, in our case every 2 seconds -> 30 (fps) * 2 = 60
        // (gop length)

        try {
            recorder.start();
        } catch (FrameRecorder.Exception e) {
            e.printStackTrace();
        }


        this.videoReady = true;

    }
    @Override
    public void record(BufferedImage bufferedImage, ByteBuffer byteBuffer){
        if (!this.videoReady) {
            initVideo();
        }
        try {
        frame = Converter.convert(byteBuffer, width, height);
        recorder.record(frame);

        ObjectMapper mapper = new ObjectMapper();
        String json= mapper.writeValueAsString(latencyTracker);
        byte[] buffer = json.getBytes();

        InetAddress address = InetAddress.getByName(IP_VIDEO);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, PORT_DATA);
        DatagramSocket datagramSocket = new DatagramSocket();
        datagramSocket.send(packet);

        } catch (FrameRecorder.Exception e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finish() {
        try {
            recorder.stop();
        } catch (FrameRecorder.Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FFmpegRecorder that = (FFmpegRecorder) o;
        return videoReady == that.videoReady &&
                currentTimeStamp == that.currentTimeStamp &&
                Objects.equals(recorder, that.recorder) &&
                Objects.equals(frame, that.frame);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recorder, frame, videoReady, currentTimeStamp);
    }
}
