**Running environment**

This was tested on an **Ubuntu 18.04.5** LTS machine.
Development was done in **Intellij 2017.3** with _OpenJDK JVM from JetBrains_


**Dependencies**
- Java **JDK** and JRE: _version 1.8.0_181_ or higher
- Apache **Maven** _version 3.6.0_ or higher
- **Docker** _version 19.03.13_ or higher

**How to use**

1. First clone the repo: `git clone https://gitlab.com/oltyx/applicationstreamingnavalcloud.git`

2. Change directory: `cd applicationstreamingnavalcloud`

3. _Setting up Janus_

I prepared a docker container for you, since building Janus takes a long time and a lot of things can go wrong.
- Build the image from the Dockerfile: `docker build --tag janus-image .` 
- Publish the exposed ports and run the container: `docker run -i -p 8188:8188 -p 7088:7088 -p 7889:7889 -p 8088:8088 -p 8089:8089 -p 8004:8004 -p 8012:8012 --detach --name janus-server janus-image bash` 
- Start the Janus WebRTC server: `docker exec janus-server /opt/janus/bin/janus`

At the beginning of the Janus output you wil also see the **JANUS_IP**.

4. Starting the rendering/streaming engine
- Download dependencies build and run: `mvn compile exec:java -Dexec.mainClass="render.Radar" -Dexec.args="[`**JANUS_IP**`] [`**VIDEO_PORT**`] [`**DATA_PORT**`]"`
- Default values: `-Dexec.args="172.17.0.2 8004 8012"`
- Port values can be found in the config file for the streaming plugin inside the Janus container: `vim /opt/janus/etc/janus/janus.plugin.streaming.jcfg`

5. Starting the WebSocket for click interactivity
- Build and run: `mvn compile exec:java -Dexec.mainClass="interactivity.WebSocketTest"`
I made it a separate program, such that you can attach a data channel functionality if you manage to setup the connection following the next skeleton.

6. Start the web application following the tutorial from the WebApp repository.

**How to use the Video Room plugin with the Gstreamer WebRTC peer implementation**

This part assumes you already executed steps **1 to 3.**

1. Setting up Gstreamer 1.18 container

- Download docker cotainer with Gstreamer 1.18: `docker pull oltyalex/gst-webrtc-1.18.0:v0.2`
- Start the container: `docker run -id --name gst-webrtc-1.18 oltyalex/gst-webrtc-1.18.0:v0.2 bash`
- Get interactive shell in the container: `docker exec -it gst-webrtc-1.18 bash`
- Path to the Python script that restream the video stream from the rendering server: `cd root/gst-examples/webrtc/janus/`
- Execute the script `python3 janusvideoroom_H264_working.py --server=ws://[`**JANUS_IP**`]:8188 gstreamer`
2. Start the rendering engine
 - Use the correct ports: `mvn compile exec:java -Dexec.mainClass="render.Radar" -Dexec.args="[`**JANUS_IP**`] [`**VIDEO_PORT**`] [`**DATA_PORT**`]"`
 - Default ports: `-Dexec.args="172.17.0.3 5200 8012"`


**Note**: **This is currently not working!** The python script that used to work(video only, no datachannel) before my last optimization [commit](3d6df68ef7b01f47718f5262f2c5d5c0d05ea675). So you would have to revert to some [previous commit](fe3d3ad0998e8cf5101b0ff0a0147cccf423bac7). The script is mostly based on [this](https://gitlab.freedesktop.org/gstreamer/gst-examples/-/blob/master/webrtc/janus/janusvideoroom.py). My guess that the restreaming does not work because of the Gstreamer pipeline. You still need to make nogatiation for the datachannel!